# Build several interesting Hello World programs

TARGETS=hello1 hello2

all: $(TARGETS)

hello1: hello1.cpp
	g++ -o hello1 hello1.cpp	

hello2: hello2.cpp
	g++ -o hello2 hello2.cpp

clean:
	rm -f $(TARGETS) *.o

