///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 04c - Hello World C++
///
/// This is an Object Oriented C++ Hello World program
///
/// @file hello2.cpp
/// @version 1.0
///  
/// @name Shaun Corpuz <corpuzsa@hawaii.edu>
/// @date 02/15/2021
///
///
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <iostream>

int main() {
   std::cout << "Hello World!" << std::endl;

   return 0;
}

